class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :title
      t.date :date
      t.string :short_description
      t.float :amount
      t.integer :status
      t.string :full_description
      t.timestamps
    end
  end
end
