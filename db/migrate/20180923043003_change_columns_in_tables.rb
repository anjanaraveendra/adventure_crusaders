class ChangeColumnsInTables < ActiveRecord::Migration[5.2]
  def change
    def up
      change_column :tentative_schedules, :value, :string
      change_column :events, :what_to_pack, :string
      change_column :events, :inclusions, :string
      change_column :events, :exclusions, :string
    end

    def down
      change_column :tentative_schedules, :value, :text
      change_column :events, :what_to_pack, :text
      change_column :events, :inclusions, :text
      change_column :events, :exclusions, :text
    end
  end
end
