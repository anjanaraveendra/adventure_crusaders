class CreateSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :settings do |t|
      t.string :key
      t.text :description
      t.string :document

      t.timestamps
    end
  end
end
