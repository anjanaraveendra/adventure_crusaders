class AddPicksToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :picks, :text, array: true, default: []
  end
end
