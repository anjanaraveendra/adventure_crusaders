class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.string :txn_id
      t.float :txn_amount
      t.string :payment_mode
      t.string :currency
      t.datetime :txn_date
      t.string :status
      t.string :res_code
      t.string :res_message
      t.string :gateway_name
      t.string :bank_txn_id
      t.string :bank_name
      t.string :checksum_cash

      t.timestamps
    end
  end
end
