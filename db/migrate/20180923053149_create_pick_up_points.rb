class CreatePickUpPoints < ActiveRecord::Migration[5.2]
  def change
    create_table :pick_up_points do |t|
      t.string :name
      t.references :event, foreign_key: true

      t.timestamps
    end
  end
end
