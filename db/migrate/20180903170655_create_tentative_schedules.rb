class CreateTentativeSchedules < ActiveRecord::Migration[5.2]
  def change
    create_table :tentative_schedules do |t|
      t.string :key
      t.text :value, array: true
      t.timestamps
    end
  end
end
