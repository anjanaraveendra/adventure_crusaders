class AddKeyToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :key, :string
  end
end
