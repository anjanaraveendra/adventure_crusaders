class AddMonthToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :month, :integer
  end
end
