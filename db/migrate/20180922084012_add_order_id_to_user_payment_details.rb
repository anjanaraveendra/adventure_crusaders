class AddOrderIdToUserPaymentDetails < ActiveRecord::Migration[5.2]
  def change
    add_column :user_payment_details, :order_id, :string
  end
end
