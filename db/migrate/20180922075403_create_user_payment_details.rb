class CreateUserPaymentDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :user_payment_details do |t|
      t.string :first_name
      t.string :last_name
      t.string :mobile_number
      t.string :email
      t.integer :head_count

      t.timestamps
    end
  end
end
