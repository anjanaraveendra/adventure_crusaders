class AddEventIdToDocuments < ActiveRecord::Migration[5.2]
  def change
    add_reference :documents, :event, foreign_key: true
  end
end
