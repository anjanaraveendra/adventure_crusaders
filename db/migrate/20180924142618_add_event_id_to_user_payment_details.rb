class AddEventIdToUserPaymentDetails < ActiveRecord::Migration[5.2]
  def change
    add_reference :user_payment_details, :event, foreign_key: true
  end
end
