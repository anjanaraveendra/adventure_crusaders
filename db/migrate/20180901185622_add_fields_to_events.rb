class AddFieldsToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :what_to_pack, :text, array: true
    add_column :events, :inclusions, :text, array: true
    add_column :events, :exclusions, :text, array: true
  end
end
