class RemoveAndReAddTextFieldsFromTables < ActiveRecord::Migration[5.2]
  def change
    remove_column :tentative_schedules, :value, :string
    remove_column :events, :what_to_pack, :string
    remove_column :events, :inclusions, :string
    remove_column :events, :exclusions, :string

    add_column :tentative_schedules, :value, :string
    add_column :events, :what_to_pack, :string
    add_column :events, :inclusions, :string
    add_column :events, :exclusions, :string
  end
end
