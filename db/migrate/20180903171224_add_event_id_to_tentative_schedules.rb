class AddEventIdToTentativeSchedules < ActiveRecord::Migration[5.2]
  def change
    add_reference :tentative_schedules, :event, foreign_key: true
  end
end
