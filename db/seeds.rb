if AdminUser.find_by(email: 'admin@adventurecrusaders.com').nil?
  AdminUser.create!(email: 'admin@adventurecrusaders.com', password: 'password', password_confirmation: 'password')
end

if Setting.count < 1
  1.upto(3).each do |i|
    image = ['2.jpg', '3.jpeg', '4.jpg', '5.jpg'].sample
    doc = Rails.root.join("app/assets/images/#{image}").open
    Setting.create(key: "Highlights#{i}", document: doc)
  end

  Setting.create(key: "email", description: 'info@adventurecrusaders.com')
  Setting.create(key: "checkout_terms", description: "I agree to Adventure Crusaders Club's Terms & Conditions and Privacy Policy")
  Setting.create(key: "contact_number", description: '+91 9164460041')
  Setting.create(key: "address", description: '1st Floor, #250 Shivakote, Bangalore: 560089')

  1.upto(6).each do |i|
    image = ['2.jpg', '3.jpeg', '4.jpg', '5.jpg'].sample
    doc = Rails.root.join("app/assets/images/#{image}").open
    Event.create(title: "Trek to somewhere #{SecureRandom.hex}",
                 short_description: "Forts always have a mysterious aura surrounding them. Most of them are reminiscences of a rich bygone era. Every fort has a history associated with it. Now if a fort exists in the midst of dense forest and atop a hill in the beautiful Western Ghats, it is bound to be a place full of history and mystery. Ballalarayana Durga fort in the Chikmagalur district of Karnataka is one such amazing fort.",
                 amount: [3000, 4000, 5000].sample,
                 status: Event.statuses.keys.sample,
                 full_description: "Ballalarayana Durga fort is famous as a trekking destination.Situated atop a hill that is 1509 metres high, the fort overlooks the town of Sunkasaale. The trek to the fort is a difficult one and presents quite a few challenges to the trekker. Fort has a mysterious aura surrounding it. Every fort has a history associated with it. Now if a fort exists in the midst of dense forest and atop a hill in the beautiful Western Ghats, it is bound to be a place full of history and mystery. Ballalarayana Durga fort in the Chikmagalur district of Karnataka is one such amazing fort.This isolated fort can only be reached by trekking through the hilly forest region. It is situated in Bettabalige that stands in the midst of Chikmagalur’s Kottegehara village and the Kalasa temple-town. It is one of the few forts that can be found in the Western Ghats. The Ballalarayana Durga fort was constructed by the wife of Veera Ballala l who was the king of the Hoysala Empire in the 12th century. It is built in the Karnata Dravida style of architecture, popular in the Hoysala Empire during their rule.There is an ancient Kalabyraveshwara temple that is situated just below the fort. The fort has lost its glory and stands as a distant reminder of its beautiful past. However surrounding green luxurious forests with steep slopes has not dimmed over time. The presence of an ever-blowing gentle breeze the place is a serene spot to relax and enjoy the beauty of nature.",
                 what_to_pack: "Personal medication (if any), Strong backpack (Preferably water proof), Fresh pair of clothes for two days., Toiletries., Mosquito Reppellent Cream",
                 inclusions: "Forest permission charges, Non A/C Transport from Bangalore to Bangalore, Accommodation, Veg Meals (2 Breakfast, 1 lunch & 1 Dinner), Basic First aid Support, Instructor",
                 exclusions: "Sunday Lunch, Snacks During Transit, Anything not mentioned above in Inclusion",
                 key: "",
                 event_type: 0,
                 duration: "AUGUST 4TH - 5TH 2018",
                 document: doc
    )
  end

  1.upto(2).each do |i|
    image = ['IMG-1.jpg', 'IMG-2.jpg', 'IMG-3.jpg', 'IMG-4.jpg', 'IMG-5.jpg', 'IMG-6.jpg'].sample
    doc = Rails.root.join("app/assets/images/#{image}").open

    Event.create(title: "Expedition Number #{i}",
                 short_description: "Forts always have a mysterious aura surrounding them. Most of them are reminiscences of a rich bygone era. Every fort has a history associated with it. Now if a fort exists in the midst of dense forest and atop a hill in the beautiful Western Ghats, it is bound to be a place full of history and mystery. Ballalarayana Durga fort in the Chikmagalur district of Karnataka is one such amazing fort.",
                 amount: [10000, 20000, 30000].sample,
                 status: 'coming_soon',
                 full_description: "",
                 what_to_pack: "Personal medication (if any), Strong backpack (Preferably water proof), Fresh pair of clothes for two days., Toiletries., Mosquito Reppellent Cream",
                 inclusions: "",
                 exclusions: "",
                 key: "Expedition#{i}",
                 event_type: 2,
                 duration: "AUGUST 4TH - 5TH 2018",
                 document: doc)

  end

  # image = ['IMG-1.jpg', 'IMG-2.jpg', 'IMG-3.jpg', 'IMG-4.jpg', 'IMG-5.jpg', 'IMG-6.jpg'].sample
  # doc = Rails.root.join("app/assets/images/#{image}").open

  # Event.create(title: "Course Number #{i}",
  #              short_description: "",
  #              amount: [10000, 20000, 30000].sample,
  #              status: 'coming_soon',
  #              full_description: "",
  #              what_to_pack: "Personal medication (if any), Strong backpack (Preferably water proof), Fresh pair of clothes for two days., Toiletries., Mosquito Reppellent Cream",
  #              inclusions: "",
  #              exclusions: "",
  #              key: "Course#{i}",
  #              event_type: 1,
  #              duration: "AUGUST 4TH - 5TH 2018",
  #              document: doc)


  { who_we_are: "Adventure Crusaders Club - provides sustainable Eco-tourism trips and adventure activities for those who wish to experience the natural beauty and culture of India, Nepal, and Bhutan - all while participating in Adventures Activities and learning nature conservation outdoors.",
    what_we_do: "All the activities of the club intended to introduce you to a new prospective towards nature. From the depths of the Bay of Bengal, to the ever green forests of Western Ghats, to the hidden Blue Mountain forests of Bhutan, Snow Caped mountains of Himalayas we go places where untouched nature is supreme.",
    from_whom_we_learn: "Visiting any country, no matter how beautiful the landscapes may be, is incomplete without experiencing the local culture. Everyone at ACC is either from one of the destination countries, or has lived in one of these countries for several years, ensuring a family-style experience for all Crusaders.",
    what_we_learn: "Participants will learn about ecosystem services, the importance of biodiversity, roles of protected areas, and principles of sustainability. The result is improved ecological literacy and a better understanding of what living sustainably truly entails."}.each do |key, value|
    Setting.create(key: key.to_s, description: value)
  end
end

if Gallery.count < 1
  1.upto(6).each do |i|
    image = ['2.jpg', '3.jpeg', '4.jpg', '5.jpg', 'IMG-1.jpg', 'IMG-2.jpg', 'IMG-3.jpg', 'IMG-4.jpg', 'IMG-5.jpg', 'IMG-6.jpg'].sample
    doc = Rails.root.join("app/assets/images/#{image}").open
    Gallery.create(key: "GalImage#{i}", image: doc)
  end
  doc = Rails.root.join("app/assets/images/screenshot-4.jpg").open
  Gallery.create(key: "GalBackground", image: doc)

  logo = Rails.root.join("app/assets/images/Main-Logo.png").open
  Gallery.create(key: "logo", image: logo)
end

if PickUpPoint.count < 1
  PickUpPoint.create(name: 'Marathalli')
  PickUpPoint.create(name: 'Kormangala')
  PickUpPoint.create(name: 'Yeshwanthpur')
end
