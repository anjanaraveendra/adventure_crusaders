# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_09_27_132353) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "comments", force: :cascade do |t|
    t.text "content"
    t.integer "commentable_id"
    t.string "commentable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "documents", force: :cascade do |t|
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "event_id"
    t.index ["event_id"], name: "index_documents_on_event_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "title"
    t.date "date"
    t.string "short_description"
    t.float "amount"
    t.integer "status"
    t.string "full_description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "document"
    t.string "duration"
    t.string "key"
    t.date "start_date"
    t.date "end_date"
    t.integer "event_type"
    t.string "what_to_pack"
    t.string "inclusions"
    t.string "exclusions"
    t.text "picks", default: [], array: true
    t.integer "parent_id"
  end

  create_table "galleries", force: :cascade do |t|
    t.string "image"
    t.string "key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "instructor_details", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_instructor_details_on_event_id"
    t.index ["user_id"], name: "index_instructor_details_on_user_id"
  end

  create_table "pick_up_points", force: :cascade do |t|
    t.string "name"
    t.bigint "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["event_id"], name: "index_pick_up_points_on_event_id"
  end

  create_table "settings", force: :cascade do |t|
    t.string "key"
    t.text "description"
    t.string "document"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tentative_schedules", force: :cascade do |t|
    t.string "key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "event_id"
    t.string "value"
    t.index ["event_id"], name: "index_tentative_schedules_on_event_id"
  end

  create_table "transactions", force: :cascade do |t|
    t.string "txn_id"
    t.float "txn_amount"
    t.string "payment_mode"
    t.string "currency"
    t.datetime "txn_date"
    t.string "status"
    t.string "res_code"
    t.string "res_message"
    t.string "gateway_name"
    t.string "bank_txn_id"
    t.string "bank_name"
    t.string "checksum_cash"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_payment_details", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "mobile_number"
    t.string "email"
    t.integer "head_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "order_id"
    t.bigint "transaction_id"
    t.bigint "event_id"
    t.index ["event_id"], name: "index_user_payment_details_on_event_id"
    t.index ["transaction_id"], name: "index_user_payment_details_on_transaction_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "first_name"
    t.string "contact_number"
    t.string "last_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "instructor", default: false
  end

  add_foreign_key "documents", "events"
  add_foreign_key "instructor_details", "events"
  add_foreign_key "instructor_details", "users"
  add_foreign_key "pick_up_points", "events"
  add_foreign_key "tentative_schedules", "events"
  add_foreign_key "user_payment_details", "events"
  add_foreign_key "user_payment_details", "transactions"
end
