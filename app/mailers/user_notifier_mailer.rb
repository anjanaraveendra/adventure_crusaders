class UserNotifierMailer < ApplicationMailer
  def get_in_touch(params)
    @email = params['email']
    @name = params['name']
    @message = params['message']
    mail(from: @email, to: to_email, subject: 'Get In Touch')
  end

  def payment_received(transaction)
    @transaction = transaction
    @user_payment_detail = transaction.user_payment_detail
    @event = @user_payment_detail.event
    mail(from: 'info@adventurecrusaders.com', to: @user_payment_detail.email, subject: 'Payment received')
  end

  private

  def to_email
    Setting.find_by(key: 'email').description
  end
end
