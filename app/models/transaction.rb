class Transaction < ApplicationRecord
  has_one :user_payment_detail
  scope :success, -> { where(status: 'TXN_SUCCESS').order('created_at desc') }
  scope :failure, -> { where.not(status: 'TXN_SUCCESS') }
end
