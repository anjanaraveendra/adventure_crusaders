require 'csv'
class CsvDb
  class << self
    def convert_save(model_name, csv_data)
      csv_file = csv_data.read.gsub /\r/, ''
      parser = CSV.parse(csv_file).to_a
      parsed_contents = parser[1..-1]
      headers = parser[0].map(&:underscore)
      parsed_contents.each do |row|
        params = Hash[headers.zip(row.map { |i| i.force_encoding('UTF-8') if i.present? })]
        event = Event.new(params)
        event.save
      end
    end
  end
end