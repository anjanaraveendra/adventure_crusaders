class UserPaymentDetail < ApplicationRecord
  belongs_to :payment_transaction, class_name: 'Transaction', foreign_key: :transaction_id, optional: true
  belongs_to :event
end
