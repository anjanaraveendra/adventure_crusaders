class PickUpPoint < ApplicationRecord
  belongs_to :event, optional: true
end
