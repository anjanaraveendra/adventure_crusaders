class Document < ApplicationRecord
  belongs_to :event
  mount_uploader :image, DocumentUploader
end
