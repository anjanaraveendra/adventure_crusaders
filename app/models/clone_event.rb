require 'csv'
class CloneEvent
  class << self
    def perform(event_id)
    	event = Event.find_by(id: event_id)
    	if event
	      event_dup = event.amoeba_dup
	      event_dup.parent_id = event.id
	      event_dup.document = event.document
	      event_dup.save
	      return event_dup
	    end
    end
  end
end