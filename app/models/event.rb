class Event < ApplicationRecord
	mount_uploader :document, DocumentUploader

  after_save :set_tentative_schedules
  before_save :set_event_details

  has_many :comments, as: :commentable
  has_many :tentative_schedules, dependent: :destroy

  belongs_to :parent, :class_name => "Event", optional: true
  has_many :events, dependent: :destroy, :foreign_key => "parent_id"

  has_many :pick_up_points, dependent: :destroy
  accepts_nested_attributes_for :pick_up_points, allow_destroy: true
  accepts_nested_attributes_for :tentative_schedules, allow_destroy: true
  has_many :documents, dependent: :destroy
  accepts_nested_attributes_for :documents, allow_destroy: true
  has_one :instructor_detail, dependent: :destroy
  accepts_nested_attributes_for :instructor_detail, allow_destroy: true

  enum event_type: %w(event course expedition)
  enum status: %w(coming_soon bookings_open ongoing completed)
  enum month: %w(January February March April May June July August September October November December)

  scope :past, -> { where('start_date < ?', Date.current).where.not(parent_id: nil) }
  scope :on_going, -> { where('start_date = ?', Date.current).where.not(parent_id: nil) }
  scope :future, -> { where('start_date > ?', Date.current).where.not(parent_id: nil) }
  scope :parent_events, -> { where(parent_id: nil) }

  amoeba do
    enable
  end

  private

  def set_tentative_schedules
    if tentative_schedules.blank? && event_type == 'event'
      tentative_schedules.create(key: 'Friday',
                                 value: "09:30 PM Pick-up from Indiranagar ESI Hospital, 09:45 PM Pick-up from New Shanthi Sagar hotel Domlur, 10:00 PM Pick-up from Lifestyle, Magrath road")
      tentative_schedules.create(key: 'Saturday',
                                 value: "06:30 AM Reach the place - Freshen up - Breakfast, 09:30 AM Trek from Sarpadhari to Mullyangiri, 02:00 PM Reach and Have packed lunch. Trek down to Bababuddhangiri, 07:00 PM Reach base camp - Dinner - Night stay")
      tentative_schedules.create(key: 'Sunday',
                                 value: "06:00AM Pack up things and Leave to water falls Reach water falls base and have fun Start back to Bangalore, 10:00PM Reach Bangalore")
    end
  end

  def set_event_details
    if event_type == 'event' || event_type.nil?
      self.what_to_pack = "Personal medication (if any),Strong backpack (Preferably water proof),Fresh pair of clothes for two days,Toiletries,Mosquito Reppellent Cream" if self.what_to_pack.blank?
      self.inclusions = "Forest permission charges,Non A/C Transport from Bangalore to Bangalore,Accommodation,Veg Meals (2 Breakfast 1 lunch & 1 Dinner),Basic First aid Support,Instructor" if self.inclusions.blank?
      self.exclusions = "Sunday Lunch, Snacks During Transit,Anything not mentioned above in Inclusion" if self.exclusions.blank?
    end
  end
end
