class User < ApplicationRecord
	def full_name
		if last_name.present?
			name = first_name + " " + last_name
		else
			name = first_name
		end
		name
	end
end
