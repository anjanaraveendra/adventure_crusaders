ActiveAdmin.register User do

	permit_params :contact_number, :email, :instructor, :first_name, :last_name

  form do |f|
    f.inputs 'Instructor Details' do
      f.input :first_name
      f.input :last_name
      f.input :email
      f.input :contact_number
      f.input :instructor, as: :boolean
    end
    f.actions
  end

end
