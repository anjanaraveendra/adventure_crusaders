ActiveAdmin.register Event do
	permit_params :id, :title, :date, :short_description, :amount, :status, :full_description, :created_at,
                :updated_at, :what_to_pack, :inclusions, :picks, :exclusions, :document, :duration, :key, :start_date, :month,
                :end_date, :parent_id, :event_type, tentative_schedules_attributes: [:id, :key, :value, :_destroy, :_create, :_update],
                documents_attributes: [:id, :image, :_destroy, :_create, :_update],
                instructor_detail_attributes: [:id, :user_id, :_destroy, :_create, :_update]


  config.batch_actions = true

  scope :parent_events
  scope :coming_soon
  scope :bookings_open
  scope :on_going
  scope :completed
  scope :course
  scope :expedition


  before_save do |event|
    event.picks = []
    event.picks = params[:event][:picks].reject { |i| i.blank? }
    event.month = 0 if params[:event][:month].blank? 
  end

  action_item only: [:index] do
    link_to('Download Sample File', download_csv_path)
  end

	form :html => { :multipart => true } do |f|
    f.inputs 'Details' do
      f.input :title
      f.input :duration
      f.input :key
      f.input :month, :as => :select, :collection => Event.months.map { |k, v| [k, k] }, selected: resource.month, include_blank: false
      f.input :short_description
      f.input :amount
      f.input :full_description
      f.input :what_to_pack
      f.input :inclusions
      f.input :exclusions
      f.input :event_type, :as => :select, :collection => Event.event_types.map { |k, v| [k, k] }, selected: resource.event_type
      f.input :status, :as => :select, :collection => Event.statuses.map { |k, v| [k, k] }
      f.input :parent_id, :as => :select, :collection => Event.where(event_type: 'event', parent_id: nil).map { |event| [event.title, event.id] }
      f.input :document, as: :file
      f.input :start_date, :as => :datepicker
      f.input :end_date, :as => :datepicker
      collected_data = PickUpPoint.all.map { |x| [x.name, x.id, { checked: f.object.picks.include?(x.id.to_s) }] }.unshift([])
      f.input :picks, as: :check_boxes, collection: collected_data
    end

    instructor_detail = resource.instructor_detail.present? ? resource.instructor_detail : resource.build_instructor_detail

    f.inputs "Account Information", for: [:instructor_detail, instructor_detail] do |j|
      j.input :user_id, :as => :select, :collection => User.where(instructor: true).map { |user| [user.full_name, user.id] }#, selected: resource.instructor_detail.first_name
    end

    f.inputs 'Tentative Schedules' do
      f.has_many :tentative_schedules, allow_destroy: true do |ff|
        ff.input :key
        ff.input :value
      end
    end

    f.inputs 'Images' do
      f.has_many :documents, allow_destroy: true do |ff|
        image = (ff.object.image.url.split('/')[-1]) if ff.object.persisted?
        ff.input :image, as: :file, :label => "Image", :hint => image
      end
    end

    f.actions
  end

  action_item :only => :index do
    link_to 'Upload CSV', :action => 'upload_csv'
    link_to 'Clone Event', :action => 'clone_event_form'
  end

  collection_action :upload_csv do
    render "admin/csv/upload_csv"
  end

  collection_action :import_csv, :method => :post do
    CsvDb.convert_save("post", params[:dump][:file])
    redirect_to :action => :index, :notice => "CSV imported successfully!"
  end

  collection_action :clone_event_form do
    render "admin/clone/clone_event"
  end

  collection_action :clone_event, :method => :post do
    event = CloneEvent.perform(params[:clone_event][:event_id])
    redirect_to admin_event_path(event), :notice => "Cloned Event successfully!"
  end

  index do
    selectable_column
    column :title
    column :duration
    column :status
    column :amount
    column :key
    column :start_date
    column :end_date
    column "Image" do |event|
      if event.document.url
        image_tag event.document.url, :width =>  '180px',:height => '120px'
      end
    end
    column "" do |resource|
      links = link_to I18n.t('active_admin.edit'), edit_resource_path(resource), :class => "member_link edit_link"
      links += link_to I18n.t('active_admin.delete'), resource_path(resource), :method => :delete, :confirm => I18n.t('active_admin.delete_confirmation'), :class => "member_link delete_link"
      links
    end
  end
end
