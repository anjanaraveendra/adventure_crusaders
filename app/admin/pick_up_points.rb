ActiveAdmin.register PickUpPoint do
  permit_params :name

  form do |f|
    f.inputs 'PickUpPoints' do
      f.input :name
    end
    f.actions
  end
end
