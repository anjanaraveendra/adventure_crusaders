ActiveAdmin.register Transaction do

  config.clear_action_items!
  scope :success, :default => true do |projects|
    Transaction.success
  end

  scope :failure, :default => true do |projects|
    Transaction.failure
  end

  index do
    selectable_column
    column 'Transaction Id' do |h|
      h.txn_id
    end
    column :txn_amount
    column :gateway_name
    column :bank_txn_id
    column :payment_mode
    column "" do |resource|
      links = link_to I18n.t('active_admin.view'), resource_path(resource), :class =>  "member_link view_link"
      links
    end
  end

  show do
    columns do
      table_for resource do |h|
        column :txn_amount
        column :currency
        column :txn_date
        column :status
        column :res_code
        column :res_message
        column :gateway_name
        column :bank_txn_id
        column :checksum_cash
        column :created_at
        column :updated_at
      end
    end

    columns do
      column do
        table_for resource.user_payment_detail do |h|
          column :id
          column :first_name
          column :last_name
          column :mobile_number
          column :email
          column :head_count
          column :order_id
        end
      end
    end
  end
end
