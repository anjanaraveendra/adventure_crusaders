ActiveAdmin.register Setting do
  config.batch_actions = false
  permit_params :key,
    :description,
    :document

  form do |f|
    f.inputs 'Details' do
      f.input :key, input_html: { disabled: object.new_record? ? false : true }
      f.input :description
      f.input :document, as: :file
    end
    f.actions
  end

  index do
    selectable_column
    column :key do | h |
      h.key
    end
    column :description do | h |
      h.description
    end
    column "Image" do |setting|
      if setting.document.present?
        image_tag setting.document.url, :width =>  '180px',:height => '120px'
      end
    end
    column "" do |resource|
      #links =  link_to I18n.t('active_admin.view'), resource_path(resource), :class =>  "member_link view_link"
      links = link_to I18n.t('active_admin.edit'), edit_resource_path(resource), :class => "member_link edit_link"
      #links += link_to I18n.t('active_admin.delete'), resource_path(resource), :method => :delete, :confirm => I18n.t('active_admin.delete_confirmation'), :class => "member_link delete_link"
      #links += link_to I18n.t("Comments",'active_admin.comment').split('.')[-1], resource_path(resource), :class => "member_link comment_link"
      links
    end
  end
end
