require Rails.root.join("lib/paytm_helper.rb")

class PaymentsController < ApplicationController
  include PaytmHelper::EncryptionNewPG
  protect_from_forgery except: [:order_processed]
  before_action :set_environment
  before_action :set_event, only: %i(proceed_to_pay)

  def proceed_to_pay
    @param_list = Hash.new
    order_id = generate_order_id#params["ORDER_ID"]
    cust_id = generate_customer_id#params["CUST_ID"]
    txn_amount = @event.amount * params[:head_count].to_f#params["TXN_AMOUNT"]
    mobile_number = params["mobile_number"]
    email = params["email"]
    payment_environment = set_environment.to_sym
    @param_list["MID"] = Rails.application.secrets[:paytm_credentials][payment_environment][:mid]
    @param_list["ORDER_ID"] = order_id
    @param_list["CUST_ID"] = cust_id
    @param_list["INDUSTRY_TYPE_ID"] = Rails.application.secrets[:paytm_credentials][payment_environment][:industry_type_id]
    @param_list["CHANNEL_ID"] = Rails.application.secrets[:paytm_credentials][payment_environment][:web][:channel_id]
    @param_list["TXN_AMOUNT"] = txn_amount
    @param_list["MOBILE_NO"] = mobile_number
    @param_list["EMAIL"] = email
    @param_list["WEBSITE"] = Rails.application.secrets[:paytm_credentials][payment_environment][:web][:website]
    @param_list["CALLBACK_URL"] = Rails.application.secrets[:paytm_credentials][payment_environment][:web][:callback_url]
    UserPaymentDetail.create(first_name: params[:first_name], last_name: params[:last_name], mobile_number: mobile_number, email: email, head_count: params[:head_count].to_i, order_id: order_id, event_id: params[:event_id])
    @checksum_hash = new_pg_checksum(@param_list, Rails.application.secrets[:paytm_credentials][payment_environment][:merchant_key]).gsub("\n",'')
    @payment_url = Rails.application.secrets[:paytm_credentials][payment_environment][:web][:payment_url]
  end

  # post /order-processed
  def order_processed
    puts "#{params.to_s}"
    @transaction = Transaction.new(transaction_params)
    @transaction.save
    @user_payment_details = UserPaymentDetail.find_by(order_id: params['ORDERID'])
    @user_payment_details.update_attributes(transaction_id: @transaction.id)
    UserNotifierMailer.payment_received(@transaction).deliver
    redirect_to payment_success_path({event_id: @user_payment_details.event_id})
  end

  # MOBILE
  # post generate-checksum
  def generate_checksum
    payment_environment = set_environment.to_sym

    params_keys_to_accept = ["MID", "ORDER_ID", "CUST_ID", "INDUSTRY_TYPE_ID", "CHANNEL_ID", "TXN_AMOUNT",
      "WEBSITE", "CALLBACK_URL", "MOBILE_NO", "EMAIL", "THEME"]
    params_keys_to_ignore = ["USER_ID", "controller", "action", "format"]

    paytmHASH = Hash.new

    paytmHASH["MID"] = Rails.application.secrets[:paytm_credentials][payment_environment][:mid]
    paytmHASH["ORDER_ID"] = params["ORDER_ID"]
    paytmHASH["CUST_ID"] = params["CUST_ID"]
    paytmHASH["INDUSTRY_TYPE_ID"] = Rails.application.secrets[:paytm_credentials][payment_environment][:industry_type_id]
    paytmHASH["CHANNEL_ID"] = Rails.application.secrets[:paytm_credentials][payment_environment][:web][:channel_id]
    paytmHASH["TXN_AMOUNT"] = params["TXN_AMOUNT"]
    paytmHASH["WEBSITE"] = Rails.application.secrets[:paytm_credentials][payment_environment][:web][:website]

    keys = params.keys
    keys.each do |key|
      if ! params[key].blank?
        puts "params[#{key}] : #{params[key]}"
        if !(params_keys_to_accept.include? key)
            next
        end
        paytmHASH[key] = params[key]
      end
    end

    mid = paytmHASH["MID"]
    order_id = paytmHASH["ORDER_ID"]

    Rails.logger.debug "paytmHASH: #{paytmHASH}"

    checksum_hash = PaytmHelper::ChecksumTool.new.get_checksum_hash(paytmHASH, Rails.application.secrets[:paytm_credentials][payment_environment][:merchant_key]).gsub("\n",'')

    # Prepare the return json.
    returnJson = Hash.new
    returnJson["CHECKSUMHASH"] =  checksum_hash
    returnJson["ORDER_ID"]     =  order_id
    returnJson["payt_STATUS"]  =  1

    Rails.logger.debug "returnJson: #{returnJson}"

    render json: returnJson
  end

  # post verify-checksum
  def verify_checksum
    payment_environment = set_environment.to_sym
    params_keys_to_ignore = ["USER_ID", "controller", "action", "format"]

    paytmHASH = Hash.new

    keys = params.keys
    keys.each do |key|
      if (params_keys_to_ignore.include? key)
        next
      end

      paytmHASH[key] = params[key]
    end
    paytmHASH = PaytmHelper::ChecksumTool.new.get_checksum_verified_array(paytmHASH, Rails.application.secrets[:paytm_credentials][payment_environment][:merchant_key])

    @response_value = paytmHASH.to_json.to_s.html_safe
  end

  private

  def generate_order_id
    "ORDER#{Time.now.to_i}"
  end

  def generate_customer_id
    SecureRandom.random_number(1_000_000)
  end

  def set_environment
    @payment_environment ||= Rails.env
  end

  def set_event
    @event = Event.find(params[:event_id])
  end

  def transaction_params
    {
      txn_id: params['TXNID'],
      txn_amount: params['TXNAMOUNT'],
      payment_mode: params['PAYMENTMODE'],
      currency: params['CURRENCY'],
      txn_date: params['TXNDATE'],
      status: params['STATUS'],
      res_code: params['RESPCODE'],
      res_message: params['RESPMSG'],
      gateway_name: params['GATEWAYNAME'],
      bank_txn_id: params['BANKTXNID'],
      bank_name: params['BANKNAME'],
      checksum_cash: params['CHECKSUMHASH']
    }
  end
end