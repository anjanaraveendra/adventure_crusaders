class HomeController < ApplicationController
  before_action :data_setting, only: %i(index)

  def terms_and_conditions
    @logo = Gallery.find_by(key: 'logo')
    @email = Setting.find_by(key: 'email').description
    @terms_and_conditions = Setting.find_by(key: 'terms_and_conditions')
    @contact_number = Setting.find_by(key: 'contact_number').description
  end

  def privacy_policy
    @email = Setting.find_by(key: 'email').description
    @logo = Gallery.find_by(key: 'logo')
    @contact_number = Setting.find_by(key: 'contact_number').description
  end

  def download_csv
    send_file(
      "#{Rails.root}/public/events_bulk.csv",
      filename: "events_bulk.csv"
    )
  end

  private

  def data_setting
    1.upto(3).map { |i| instance_variable_set("@highlights_#{i}", Setting.find_by(key: "Highlights#{i}")) }
    @email = Setting.find_by(key: 'email').description
    @address = Setting.find_by(key: 'address').description
    @contact_number = Setting.find_by(key: 'contact_number').description
    #1.upto(6).map { |i| instance_variable_set("@events_#{i}", Event.find_by(key: "Event#{i}")) }
    @events = Event.bookings_open.where.not(parent_id: nil)#.or(Event.coming_soon)
    1.upto(2).map { |i| instance_variable_set("@expedition_#{i}", Event.find_by(key: "Expedition#{i}")) }
    %w(who_we_are what_we_do from_whom_we_learn what_we_learn).each do |key|
    	instance_variable_set("@#{key}", Setting.find_by(key: key))
    end
    1.upto(6).map { |i| instance_variable_set("@gal_image_#{i}", Gallery.find_by(key: "GalImage#{i}")) }
    @gal_background = Gallery.find_by(key: "GalBackground")
    @logo = Gallery.find_by(key: 'logo')
  end
end
