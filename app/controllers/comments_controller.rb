class CommentsController < ApplicationController
  before_action :load_commentable
  before_action :find_event

  def index
    @comments = @commentable.comments
  end

  def new
  end

  def create
    @comment = @event.comments.new(comment_params)
    if @comment.save
      flash[:success] = "Comment added successfully"
      redirect_to event_path(@event)
    end
  end

  private

  def comment_params
    params.require(:comment).permit!
  end

  def load_commentable
    resource, id = request.path.split('/')[1,2]
    @commentable = resource.singularize.classify.constantize.find(id)
  end

  def find_event
    @event = Event.find_by(id: params[:event_id])
  end
end
