class EventsController < ApplicationController
  before_action :find_event, only: [:show]

  def show
    @contact_number = Setting.find_by(key: 'contact_number').description
    @logo = Gallery.find_by(key: 'logo')
    @email = Setting.find_by(key: 'email').description
    @commentable = @event
    @comments = @commentable.comments
    @comment = Comment.new
  end

  def index
    @completed_events = Event.event.completed.where.not(parent_id: nil).group_by { |e| Event.months[e.month] }#.sort.to_h
    @bookings_open_events = Event.event.bookings_open.where.not(parent_id: nil).group_by { |e| Event.months[e.month] }#.sort.to_h
    @coming_soon_events = Event.event.coming_soon.where.not(parent_id: nil).group_by { |e| Event.months[e.month] }#.sort.to_h
    @ongoing_events = Event.event.ongoing.where.not(parent_id: nil).group_by { |e| Event.months[e.month] }#.sort.to_h
  end

  private

  def find_event
    @event = Event.find_by(id: params[:id])
  end
end
