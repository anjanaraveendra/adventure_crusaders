class ContactAdminController < ApplicationController

  def contact
    UserNotifierMailer.get_in_touch(params).deliver
    redirect_to root_path
  end
end
