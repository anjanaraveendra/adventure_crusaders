class CheckoutController < ApplicationController
  def index
    @event = Event.find(params[:event_id])
    @checkout_terms = Setting.find_by(key: 'checkout_terms')
    @pick_up_points = PickUpPoint.where(id: @event.picks)
  end
end