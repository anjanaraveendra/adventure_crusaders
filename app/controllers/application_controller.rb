class ApplicationController < ActionController::Base
  before_action :set_data

  def set_data
    @email = Setting.find_by(key: 'email').description
    @logo = Gallery.find_by(key: 'logo')
    @contact_number = Setting.find_by(key: 'contact_number').description
  end
end
