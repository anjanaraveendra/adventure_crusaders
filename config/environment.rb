# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!
env_file = File.join(Rails.root, 'config', 'secrets.yml')
env_values = YAML.load(File.open(env_file))

ActionMailer::Base.smtp_settings = {
  :user_name => env_values['MAIL_USERNAME'],
  :password => env_values['MAIL_PASSWORD'],
  :domain => 'localhost',
  :address => 'smtp.sendgrid.net',
  :port => 587,
  :authentication => :plain,
  :enable_starttls_auto => true
}
