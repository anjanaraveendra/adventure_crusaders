Rails.application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'
  get 'terms_and_conditions', to: :show, controller: 'home'
  get 'privacy_policy', to: :show, controller: 'home'
  get 'download_csv', to: :show, controller: 'home'

  get "/checkout" => "checkout#index", as: :checkout
  post "/proceed-to-pay" => "payments#proceed_to_pay", as: :proceed_to_pay
  post "/order-processed" => "payments#order_processed", as: :order_processed
  get "/payment_success" => "payments#payment_success"

  resources :landing

  resources :events do
    resources :comments
  end
  resources :contact_admin do
    collection do
      post :contact
    end
  end
end
